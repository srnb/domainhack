package tf.bug.domainhack

import java.net.URL

import scala.io.Source

case class PublicList(listUrl: String, private val beginDelim: String, private val endDelim: String, private val comment: String) {

  private val url = new URL(listUrl)
  private val conn = url.openConnection()
  conn.addRequestProperty("User-Agent", "tf.bug.domainhack/0.1.0")
  val tlds: List[String] = Source.fromInputStream(conn.getInputStream, "UTF-8")
    .getLines()
    .toList
    .dropWhile(_ != beginDelim)
    .reverse
    .dropWhile(_ != endDelim)
    .filter(_.nonEmpty)
    .filter(!_.startsWith(comment))

}
