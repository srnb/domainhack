package tf.bug.domainhack

import java.util.regex.Pattern

object DomainHack {

  lazy val defaultList = PublicList("https://publicsuffix.org/list/public_suffix_list.dat", "// ===BEGIN ICANN DOMAINS===", "// ===END ICANN DOMAINS===", "//")

  def apply(solution: String, list: PublicList = defaultList): List[(String, String, String)] = {
    val tlds = list.tlds
    val found = findTldsIn(solution, tlds)
    found.map {
      case (start, tld) =>
        val beginning = solution.take(start)
        val middle = tld
        val end = solution.drop(start + tld.length)
        (beginning, middle, end)
    }
  }

  def findTldsIn(solution: String, tlds: List[String]): List[(Int, String)] = {
    tlds.flatMap(tld => {
      val tldMatches = Pattern.quote(tld.replace(".", "")).r.findAllMatchIn(solution)
      tldMatches.map(m => (m.start, tld))
    })
  }

}
